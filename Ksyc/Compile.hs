module Ksyc.Compile where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Data.Text

import Ksyc.Parser
import Ksyc.Syntax

-- Special vars:
--  _io : seems to be a record of at least
--          * pos - the current byte offset in the stream (is this perhaps an offset local to the type rather than absolute in the stream)?
--          * size - maybe the size of the type or something?
--  _index : not sure what this is
--  _ : The element just parsed (e.g. used in predicates? I presume 'if' etc?).
newtype Context = Context (Map Text StructParser)

compileEnumSpec :: EnumSpec -> (Context, Stmt v)
compileEnumSpec e = undefined

compileEnumSpecs :: Map Text EnumSpec -> (Context, Stmt v)
compileEnumSpecs e = undefined

compileTypeSpec :: TypeSpec -> (Context, Stmt v)
compileTypeSpec (TypeSpec m doc seq instances enums tys params) = undefined

compileTypes :: Map Text TypeSpec -> Context
compileTypes ts = undefined

compileNamedAttrSeq :: NamedAttrSpec -> Stmt v
compileNamedAttrSeq (NamedAttrSpec name a) = undefined

compileAttrSeq :: AttrSpec -> Stmt v
compileAttrSeq (AttrSpec doc docRef contents ty p origId sz rtag rexpr runtil) = undefined

compileSeq :: [AttrSpec] -> Stmt v
compileSeq ss = undefined

compile :: Ksy -> (Context, Stmt v)
compile (Ksy meta doc docRef seq types instances enums) =
  let ctx = maybe (Context Map.empty) compileTypes types
  in (ctx, maybe Return compileSeq seq)


{-#LANGUAGE OverloadedStrings #-}
module Ksyc.Parser where

import Control.Applicative
import Control.Monad.Identity

import Data.Bifunctor (first)
import Data.Char (isAlpha, isAlphaNum)
import qualified Data.HashSet as HashSet
import qualified Data.Map as Map
import Data.Map (Map)
import Data.String (IsString)
import Data.Scientific (floatingOrInteger)
import Data.Text hiding (foldr)
import Data.Void
import Data.Yaml hiding (Number)
import qualified Data.Yaml as Yaml

import Network.URL (URL, importURL)

import qualified Text.Megaparsec as MP
import qualified Text.Megaparsec.Parsers as MPP

import Text.Parser.Char hiding (string)
import Text.Parser.Combinators
import Text.Parser.Expression
import Text.Parser.Token hiding (integer)
import qualified Text.Parser.Token as Tok
import Text.Parser.Token.Highlight
import Text.Parser.Token.Style (emptyOps)

type ExpParser = MPP.ParsecT Void Text Identity

newtype IdentText = IdentText { fromIdentText :: Text }
  deriving (Eq, Show)
data Encoding = Ascii | Utf8 | Utf16le | Utf16be | Utf32le | Utf32be | IanaCharSet Text
  deriving (Eq, Show)
data Endianness = LittleEndian | BigEndian
  deriving (Eq, Show)
newtype FilePathList = FilePathList [FilePath]
  deriving (Eq, Show)
newtype MetaFileExt = MetaFileExt (Either Text [Text])
  deriving (Eq, Show)
data Meta = Meta
  { metaId :: IdentText
  , metaTitle :: Maybe Text
  , metaApplication :: Maybe Text
  , metaXRef :: Maybe Value
  , metaImports :: FilePathList
  , metaEncoding :: Maybe Encoding
  , metaEndian :: Maybe Endianness
  , metaKsVersion :: Maybe Value
  , metaKsDebug :: Bool
  , metaKsOpaqueTypes :: Bool
  , metaLicense :: Maybe Text
  , metaFileExtension :: Maybe MetaFileExt }
  deriving (Eq, Show)
newtype Doc = Doc { fromDoc :: Text }
  deriving (Eq, Show)
data DocRef = DocRef (Maybe URL) Text
  deriving (Eq, Show)
data ProcessingSpec = XorProcess Exp
                    | RolProcess Exp
                    | ZlibProcess
  deriving (Eq, Show)
data Signedness = Signed | Unsigned
  deriving (Eq, Show)
data IntTy = IntTy Signedness Int Endianness
  deriving (Eq, Show)
data FloatTy = FloatTy Int Endianness
  deriving (Eq, Show)
data Lit
  = ZLit Integer
  | BLit Bool
  | FLit Double
  deriving (Eq, Show)
data UnaryOp = Inverse | LNot | BNot
  deriving (Eq, Show)
data BinOp
  = Div | Mul | Mod
  | Sub | Add
  | Shl | Shr
  | Gt | Gte | Lt | Lte
  | Neq | Eql
  | BAnd
  | BXor
  | BOr
  | LAnd
  | LOr
  deriving (Eq, Show)
newtype Ident = Ident { fromIdent :: Text }
  deriving (Eq, Show)
data Term = Lit Lit | Var Ident | ProjectedField Exp [Ident]
  deriving (Eq, Show)
data Exp
  = Term Term
  | If Exp Exp Exp
  | BOp BinOp Exp Exp
  | UOp UnaryOp Exp
  | ProjectField Exp Ident
  | Subscript Exp Exp
  | App Exp [Exp]
  deriving (Eq, Show)
--data AttrType
--  = DecoratedByteArray { byteArraySize :: Maybe Exp
--                       , byteArraySizeEos :: Maybe Exp
--                       , byteArrayProcess :: Maybe ProcessingSpec }
--  | DecoratedInteger { integerEnum :: Maybe EnumSpec }
--  | DecoratedString { stringSize :: Maybe Exp
--                    , stringSizeEos :: Maybe Exp
--                    , stringEncoding :: Maybe Encoding }
--  | DecoratedStrz { strzTerminator :: Integer
--                  , strzConsume :: Bool    -- default true
--                  , strzInclude :: Bool    -- default false
--                  , strzEosError :: Bool } -- default true
--  | RawType TypeSpec
--  | NamedType Text
--  deriving (Eq, Show)
data RepeatTag = EosTag | ExprTag | UntilTag
  deriving (Eq, Show)
data RepeatExp = RepeatEos | RepeatExact Exp | RepeatUntil Exp
  deriving (Eq, Show)
data NamedAttrSpec = NamedAttrSpec IdentText AttrSpec -- FIXME probably more possible fields?
  deriving (Eq, Show)
data VarDecl = VarDecl IdentText TyExp (Maybe Doc)
  deriving (Eq, Show)
data TyExp = TApp Ident [Exp]
  deriving (Eq, Show)
data AttrSpec = AttrSpec -- FIXME probably more possible fields?
  { attrSpecDoc :: Maybe Doc
  , attrSpecDocRef :: Maybe DocRef
  , attrSpecContents :: Maybe DocRef
  , attrSpecTy :: Maybe TyExp
  , attrSpecIf :: Maybe Exp
  , attrSpecOrigId :: Maybe Text
  , attrSpecSize :: Maybe Exp
  , attrSpecRepeat :: Maybe RepeatTag
  , attrSpecRepeatExpr :: Maybe Exp -- These are used for byte arrays, but can they be present for non-byte arrays?
  , attrSpecRepeatUntil :: Maybe Exp } -- These are used for byte arrays, but can they be present for non-byte arrays?
  deriving (Eq, Show)
data TypeSpec = TypeSpec
  { typeSpecMeta :: Maybe Meta
  , typeSpecDoc :: Maybe Doc
  , typeSpecSeq :: [NamedAttrSpec]
  , typeSpecInstances :: Maybe (Map Text AttrInstance)
  , typeSpecEnums :: Maybe (Map Text EnumSpec)
  , typeSpecTypes :: Maybe (Map Text TypeSpec)
  , typeSpecParams :: Maybe [VarDecl]
  }
  deriving (Eq, Show)
newtype AttrInstance = AttrInstance (InstanceSpec AttrSpec)
  deriving (Eq, Show)
data InstanceSpec a = InstanceSpec a
                                   (Maybe Exp) -- pos : seems to be the (absolute) position to start parsing from as (i.e. as an offset from the start of the stream)?
                                   (Maybe Exp) -- io :
                                   (Maybe Exp) -- value : This calculates (i.e. is an expression) and stores the specified value, it's just a function.
  deriving (Eq, Show)
data EnumDecl = EnumDecl IdentText (Maybe Doc) (Maybe DocRef)
  deriving (Eq, Show)
newtype EnumSpec = EnumSpec { fromEnumSpec :: Map Int EnumDecl }
  deriving (Eq, Show)

data Ksy = Ksy
  { ksyMeta :: Meta
  , ksyDoc :: Maybe Doc
  , ksyDocRef :: Maybe DocRef
  , ksySeq :: Maybe [AttrSpec]
  , ksyTypes :: Maybe (Map Text TypeSpec)
  , ksyInstances :: Maybe (Map Text AttrInstance)
  , ksyEnums :: Maybe (Map Text EnumSpec) }
  deriving (Eq, Show)

type FieldParser a = Object -> Parser a

instance FromJSON FilePathList where
  parseJSON = fmap (FilePathList . fmap unpack) . (parseJSON :: Value -> Parser [Text])

instance FromJSON RepeatTag where
  parseJSON = withText "repeat tag 'eos', 'until', or 'expr'" (pure . repeatTagFromText)

repeatTagFromText :: Text -> RepeatTag
repeatTagFromText t
  | t == "eos"   = EosTag
  | t == "expr"  = ExprTag
  | t == "until" = UntilTag

instance FromJSON Doc where
  parseJSON = withText "a document string" (pure . Doc)

instance FromJSON DocRef where
  parseJSON = withText "a document reference string" (pure . DocRef Nothing {-FIXME-})

instance FromJSON Exp where
  parseJSON n = case n of
    Bool b -> pure (Term (Lit (BLit b)))
    Object m -> fail "expected expression instead of object"
    Array ns -> fail "expected expression instead of array"
    Yaml.Number r -> pure (either (Term . Lit . FLit) (Term . Lit . ZLit) (floatingOrInteger r))
    String t -> exprFromText t
    Null -> fail "expected expression instead of null"

prettyExprParserError e = "expected a valid expression when parsing field " ++ MP.errorBundlePretty e

exprFromText :: Text -> Parser Exp
exprFromText t = case MP.parse (MPP.unParsecT (expr :: ExpParser Exp)) "" t of
  Left e -> fail (prettyExprParserError e)
  Right v -> pure v

instance FromJSON TyExp where
  parseJSON = withText "type expression" tyExprField

tyExpr :: (Monad m, TokenParsing m) => m TyExp
tyExpr = TApp <$> identExp <*> option [] (parens (expr `sepBy` comma))

tyExprField :: Text -> Parser TyExp
tyExprField t = case MP.parse (MPP.unParsecT (tyExpr :: ExpParser TyExp)) "" t of
  Left e -> fail (prettyExprParserError e)
  Right v -> pure v

instance FromJSON Encoding where
  parseJSON = withText "an encoding" (pure . encodingFromText)

instance FromJSON Endianness where
  parseJSON = withText "endianness 'be' or 'le'" (\s -> case endiannessFromText s of
    Just x -> pure x
    Nothing -> fail ("unknown endianness `" ++ unpack s ++ "'"))

instance FromJSON IdentText where
  parseJSON = withText "identifier" (pure . IdentText)

parseFilePath :: Value -> Parser FilePath
parseFilePath = withText "filepath" (pure . unpack)

encodingFromText :: Text -> Encoding
encodingFromText t
  | t == "ASCII"    = Ascii
  | t == "UTF-8"    = Utf8
  | t == "UTF-16LE" = Utf16le
  | t == "UTF-16BE" = Utf16be
  | t == "UTF-32LE" = Utf32le
  | t == "UTF-32BE" = Utf32be
  | otherwise       = IanaCharSet t

endiannessFromText :: Text -> Maybe Endianness
endiannessFromText t
  | t == "le" = Just LittleEndian
  | t == "be" = Just BigEndian
  | otherwise = Nothing

fileExtension :: Value -> Parser (Either Text [Text])
fileExtension n = Left <$> parseJSON n
              <|> Right <$> parseJSON n

namedAttrSpec :: FieldParser NamedAttrSpec
namedAttrSpec m = NamedAttrSpec
  <$> m .: "id"
  <*> attrSpec m

instance FromJSON Meta where
  parseJSON = withObject "meta" meta

instance FromJSON NamedAttrSpec where
  parseJSON = withObject "attribute spec with id" namedAttrSpec

instance FromJSON AttrSpec where
  parseJSON = withObject "attribute spec" attrSpec

instance FromJSON EnumDecl where
  parseJSON v = withObject "verbose enum value identifier" enumDecl v
            <|> EnumDecl <$> parseJSON v <*> pure Nothing <*> pure Nothing

instance FromJSON EnumSpec where
  parseJSON = fmap EnumSpec . parseJSON

instance FromJSON TypeSpec where
  parseJSON = withObject "type spec" typeSpec

instance FromJSON VarDecl where
  parseJSON = withObject "variable declaration" varDecl

instance FromJSON AttrInstance where
  parseJSON = withObject "attr spec instance" (fmap AttrInstance . attrInstance attrSpec)

attrInstance :: FieldParser a -> FieldParser (InstanceSpec a)
attrInstance p m = InstanceSpec
  <$> p m
  <*> m .:? "pos"
  <*> m .:? "io"
  <*> m .:? "value"

instance FromJSON MetaFileExt where
  parseJSON = fmap (MetaFileExt . Left) . parseJSON

instance FromJSON Ksy where
  parseJSON t = flip (withObject "kaitai struct") t $ \m -> Ksy
    <$> m .: "meta"
    <*> m .:? "doc"
    <*> m .:? "doc-ref"
    <*> m .:? "seq"
    <*> m .:? "types"
    <*> m .:? "instances"
    <*> m .:? "enums"

enumDecl :: FieldParser EnumDecl
enumDecl m = EnumDecl
  <$> m  .: "id"
  <*> m  .:? "doc"
  <*> m  .:? "doc-ref"

-- FIXME check tag against repeat fields
attrSpec :: FieldParser AttrSpec
attrSpec = rawAttrSpec

rawAttrSpec :: FieldParser AttrSpec
rawAttrSpec m = AttrSpec
  <$> m .:? "doc"
  <*> m .:? "doc-ref"
  <*> m .:? "contents"
  <*> m .:? "type"
  <*> m .:? "if"
  <*> m .:? "-orig-id"
  <*> m .:? "size"
  <*> m .:? "repeat"
  <*> m .:? "repeat-expr"
  <*> m .:? "repeat-until"

varDecl :: FieldParser VarDecl
varDecl m = VarDecl
  <$> m .: "id"
  <*> m .: "type"
  <*> m .:? "doc"

typeSpec :: FieldParser TypeSpec
typeSpec m = TypeSpec
  <$> m .:? "meta"
  <*> m .:? "doc"
  <*> m .:? "seq" .!= []
  <*> m .:? "instances"
  <*> m .:? "enums"
  <*> m .:? "types"
  <*> m .:? "params"

meta :: FieldParser Meta
meta m = Meta
  <$> m .: "id"
  <*> m .:? "title"
  <*> m .:? "application"
  <*> m .:? "xref"
  <*> m .:? "imports" .!= FilePathList []
  <*> m .:? "encoding"
  <*> m .:? "endian"
  <*> m .:? "ks-version"
  <*> m .:? "ks-debug" .!= False
  <*> m .:? "ks-opaque-types" .!= False
  <*> m .:? "license"
  <*> m .:? "file-extension"

expr :: (Monad m, TokenParsing m) => m Exp
expr = do t <- term
          ternary t <|> pure t
  where ternary :: (Monad m, TokenParsing m) => Exp -> m Exp
        ternary t = If t <$> (textSymbol "?" *> expr <* textSymbol ":") <*> expr

identExp :: (TokenParsing m, Monad m) => m Ident
identExp = Ident <$> ident identStyle

identStyle :: CharParsing m => IdentifierStyle m
identStyle = IdentifierStyle
  { _styleName = "expression identifier"
  , _styleStart = satisfy (\c -> isAlpha c || c == '_')
  , _styleLetter = satisfy (\c -> isAlphaNum c || c == '_')
  , _styleReserved = HashSet.empty
  , _styleHighlight = Identifier
  , _styleReservedHighlight = ReservedIdentifier }

term :: (Monad m, TokenParsing m) => m Exp
term = buildExpressionParser exprTable projExp

projExp :: (Monad m, TokenParsing m) => m Exp
projExp = postfixExp =<< atom

postfixExp :: (Monad m, TokenParsing m) => Exp -> m Exp
postfixExp a = (do a' <- choice [projection a
                                ,subscript a
                                ,app a]
                   postfixExp a')
           <|> pure a
   where projection a = ProjectField a <$> (dot *> identExp)
         subscript a = Subscript a <$> brackets expr
         app a = App a <$> parens (expr `sepBy` comma)

boolean :: (Monad m, TokenParsing m) => m Bool
boolean = True  <$ reserveText identStyle "true"
      <|> False <$ reserveText identStyle "false"

atom :: (Monad m, TokenParsing m) => m Exp
atom = parens expr
   <|> (Term . Var <$> identExp)
   <|> (Term . Lit . ZLit <$> natural)
   <|> (Term . Lit . FLit <$> double)
   <|> (Term . Lit . BLit <$> boolean)

exprTable :: (Monad m, TokenParsing m) => [[Operator m Exp]]
exprTable = [[prefix "-" (UOp Inverse)
             ,prefix "!" (UOp LNot)
             ,prefix "not" (UOp LNot)
             ,prefix "~" (UOp BNot)]
            ,[binary "/" (BOp Div) AssocLeft
             ,binary "*" (BOp Mul) AssocLeft
             ,binary "%" (BOp Mod) AssocLeft]
            ,[binary "-" (BOp Sub) AssocLeft
             ,binary "+" (BOp Add) AssocLeft]
            ,[binary "<<" (BOp Shl) AssocLeft
             ,binary ">>" (BOp Shr) AssocLeft]
            ,[binary ">=" (BOp Gte) AssocNone
             ,binary ">"  (BOp Gt)  AssocNone
             ,binary "<=" (BOp Lte) AssocNone
             ,binary "<"  (BOp Lt)  AssocNone]
            ,[binary "!=" (BOp Eql)  AssocNone
             ,binary "==" (BOp Neq) AssocNone]
            ,[binary "!=" (BOp Eql)  AssocNone
             ,binary "==" (BOp Neq) AssocNone]
            ,[binary "&" (BOp BAnd)  AssocLeft]
            ,[binary "^" (BOp BXor)  AssocLeft]
            ,[binary "|" (BOp BOr)  AssocLeft]
            ,[binary "&&" (BOp LAnd)  AssocLeft
             ,binary "and" (BOp LAnd)  AssocLeft]
            ,[binary "||" (BOp LOr)  AssocLeft
             ,binary "or" (BOp LOr)  AssocLeft]]
  where reservedOp = reserve emptyOps { _styleReserved = HashSet.fromList reservedOps }
        reservedOps = ["or", "and", "not"]
        binary name f = Infix (f <$ reservedOp name)
        prefix name f = Prefix (f <$ reservedOp name)

{-#LANGUAGE GADTs, DataKinds, KindSignatures#-}
module Ksyc.Syntax where

newtype Stream = Stream Int -- FIXME
  deriving (Eq, Show)
data Ty
  = TyInt
  | TyBool
  | TyArray Ty
  deriving (Eq, Show)
data Lit (t :: Ty) where
  BLit :: Bool -> Lit TyBool
  ZLit :: Bool -> Lit TyInt
  ALit :: [Lit t] -> Lit (TyArray t)
data BinOp t where
  AddOp :: BinOp TyInt
  AndOp :: BinOp TyBool
  OrOp :: BinOp TyBool
data UnaryOp t where
  NotOp :: UnaryOp TyBool
  InvOp :: UnaryOp TyInt
data Exp t where
  BOp :: BinOp t -> Exp t -> Exp t -> Exp t
  UOp :: UnaryOp t -> Exp t -> Exp t -> Exp t
  Lit :: Lit t -> Exp t
data Stmt v where
  Bind :: Stmt v -> (v -> Stmt v) -> Stmt v
  Take :: Exp TyInt -> Stmt v -> Stmt v
  TakeWhile :: Exp TyBool -> Stmt v -> Stmt v
  Repeat :: Exp t -> Stmt v -> Stmt v
  Expect :: Exp (TyArray TyInt) -> Stmt v -> Stmt v
  If :: Exp TyBool -> Stmt v -> Stmt v -> Stmt v
  Return :: Stmt v

data StructParser = StructParser () -- "types" in the ksy sense are really parsers as far as I can tell.
data Fn = Fn ()

{-#LANGUAGE QuasiQuotes #-}
module Main where

import Prelude hiding (putStrLn, readFile)

import qualified Data.ByteString as BS
import Data.Text hiding (foldr)
import Data.Text.Lazy (toStrict)
import Data.Text.IO (putStrLn)
import Data.Yaml

import Text.Pretty.Simple

import System.Environment (getArgs)
import System.Console.Docopt hiding (ParseError)

import Ksyc.Parser

usageMsg :: Docopt
usageMsg = [docopt|
ksyc version 1.0.0

Usage:
  ksyc <file>
|]

decodeKsy :: BS.ByteString -> Either ParseException Ksy
decodeKsy = decodeEither'

parseKsy :: BS.ByteString -> Text
parseKsy s = either (pack . prettyPrintParseException) (toStrict . pShow) (decodeKsy s)

main :: IO ()
main = do
  args <- parseArgsOrExit usageMsg =<< getArgs
  f <- getArgOrExitWith usageMsg args (argument "file")
  putStrLn =<< parseKsy <$> BS.readFile f
  return ()

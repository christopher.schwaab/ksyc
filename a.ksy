meta:
  id: ds_store
  title: macOS '.DS_Store' format
  license: MIT
  ks-version: 0.8.8
  encoding: UTF-8
  endian: be
enums:
  file_type:
    0: normal
    1: transaction_log
  file_format:
    1: direct_memory_load
